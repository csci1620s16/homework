/*
 * This simple class creates a list and then prints it to the screen.
 * **Note that it prints backward**
 * 
 * After the for loop, create your own custom loop (recursion) by repeatedly calling the same method.
 * **Yours should also print backward**
 * 
 * 
 * Your answer should follow the pattern we followed in class.
 * As a reminder, go to:
 * bit.ly/1620S16SRC
 * and click on Day13BucketList
 *
 */
public class D13Recursion {
	public static void main(String[] args) {
		new D13Recursion();
	}
	
	String[] list = new String[4];
	
	public D13Recursion() {
		
		list[0] = "Eeny";
		list[1] = "Meeny";
		list[2] = "Miny";
		list[3] = "Moe";
		
		for(int i = 3; i >= 0; --i)
		{
			System.out.println(list[i]);
		}
		
		/* Put your code after this comment */
		
	}
}
